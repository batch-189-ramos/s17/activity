/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/
	
	//first function here:
	function myNameIs () {
		let myNameIs = prompt('What is your name?');
	};
	myNameIs();

	function myNameIsRupert () {
		console.log('Hello, my name is Rupert!');
	};
	myNameIsRupert();

	function myAgeIs () {
		console.log('My age is 30 years old');
	};
	myAgeIs();

	function myCityIs () {
		console.log('I live in Caloocan City');
	};
	myCityIs();
/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

	//second function here:

	function myFaveBand1 () {
		console.log('1. Eraserheads');
	};
	myFaveBand1();

	function myFaveBand2 () {
		console.log('2. Paramore');
	};
	myFaveBand2();

	function myFaveBand3 () {
		console.log('3. Imago');
	};
	myFaveBand3();

	function myFaveBand4 () {
		console.log('4. Rivermaya');
	};
	myFaveBand4();

	function myFaveBand5 () {
		console.log('5. Moonstar 88');
	};
	myFaveBand5();


/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	
	//third function here:
	function myFaveMovie1 () {
		console.log('1. Weathering with you');
	};
	myFaveMovie1();

	function movie1Rating () {
		console.log('Rotten Tomatoes rating is: 92%');
	};
	movie1Rating();

	function myFaveMovie2 () {
		console.log('2. Howls Moving Castle');
	};
	myFaveMovie2();

	function movie2Rating () {
		console.log('Rotten Tomatoes rating is: 87%');
	};
	movie2Rating();

	function myFaveMovie3 () {
		console.log('3. Spirited Away');
	};
	myFaveMovie3();

	function movie3Rating () {
		console.log('Rotten Tomatoes rating is: 97%');
	};
	movie3Rating();

	function myFaveMovie4 () {
		console.log('4. Inside Out');
	};
	myFaveMovie4();

	function movie4Rating () {
		console.log('Rotten Tomatoes rating is: 98%');
	};
	movie4Rating();

	function myFaveMovie5 () {
		console.log('5. Dr. Strange in the Multiverse of Madness');
	};
	myFaveMovie5();

	function movie5Rating () {
		console.log('Rotten Tomatoes rating is: 74%');
	};
	movie5Rating();
/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/

// printUsers();
	let printFriends = function printUsers(){
	alert("Hi! Please add the names of your friends.");
	let friend1 = prompt("Enter your first friend's name:"); 
	let friend2 = prompt("Enter your second friend's name:"); 
	let friend3 = prompt("Enter your third friend's name:");

	console.log('I am friend with: ' + friend1);
	console.log('I am friend with: ' + friend2); 
	console.log('I am friend with: ' + friend3); 
	// console.log(friends); 
};

	printFriends()

/*console.log(friend1);
console.log(friend2);*/